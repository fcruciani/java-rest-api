/*
Example Wrapper: for dghclient.jar

*/
package com.serg.rest;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import com.google.gson.Gson;
import com.iplus.RESTLayer.DateUtils;
import com.iplus.RESTLayer.RESTTasks;
import com.iplus.RESTLayer.Request;
import com.iplus.RESTLayer.Response;
import com.iplus.RESTLayer.UserManager;
import com.iplus.RESTLayer.cache.persistence.Settings;
import com.iplus.RESTLayer.cache.persistence.SettingsDBEntry;
import com.iplus.RESTLayer.cache.persistence.UserDBEntry;
import com.iplus.RESTLayer.callbacks.AddEventsCallback;
import com.iplus.RESTLayer.callbacks.GetEventsCallback;
import com.iplus.RESTLayer.callbacks.RefreshTokenCallback;
import com.iplus.RESTLayer.exceptions.HTTPException;
import com.iplus.RESTLayer.marshalling.model.ERelations;
import com.iplus.RESTLayer.marshalling.model.Event;
import com.iplus.RESTLayer.marshalling.model.Events;
import com.iplus.RESTLayer.marshalling.model.Link;
import com.iplus.RESTLayer.marshalling.model.Methods;
import com.serg.model.RawDataEvent;
import com.serg.model.RawDataEvents;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Federico on 24/11/2017.
 */

public class LiteClient extends RESTTasks {

    private static LiteClient mInstance;

    LiteClient() {

    }

    static {
        mInstance = new LiteClient();
    }

    public static LiteClient getInstance() {
        return mInstance;
    }

    public void refreshToken(Context ctx, final RefreshTokenCallback callback)
    {
        UserManager mml = UserManager.getInstance();
        if( !mml.isLoggedIn(ctx) )
        {
            Exception e = new Exception("The user is not logged in!");
            callback.onRefreshTokenError(e);
            return;
        }

        UserDBEntry u = new UserDBEntry(ctx);

        try
        {
            Log.d("TOKEN TEST", u.expire);
            Date expireDate = DateUtils.toDate(u.expire);

            Calendar c = Calendar.getInstance();
            //one minute from now
            c.add(Calendar.MINUTE, 2);
            Date currentDate = c.getTime();
            Log.d("TOKEN TEST", DateUtils.toISO8601(currentDate));
            if( currentDate.before(expireDate) )
            {
                //if token is not expiring 1 minute from now
                //no need to refresh
                Log.d("TEST", "No need to refresh token");
                callback.onRefreshTokenSuccess(u.token);
                return;
            }

        }
        catch (ParseException e1) {
            // Serve solo ad evitare il crash. In caso di problemi vai avanti
            e1.printStackTrace();
        }

        final class ActualRestTaskCallback extends RESTTasks.RestTaskCallback
        {
            private Context context;

            public ActualRestTaskCallback( Context ctx ){
                context = ctx;
            }

            @Override
            public void onTaskComplete(Response response) {

                if( response.hasHTTPException())
                {
                    callback.onRefreshTokenHTTPError(response.getException());
                    return;
                }

                // Recuperare l'autorizzazione e richiedere l'utente
                try
                {
                    Map<String, List<String>> headersMap = response.getHeaders();

                    List<String> authorizedByList = headersMap.get("Authorized-By");

                    String token = authorizedByList.get(0);

                    List<String> expires = headersMap.get("Expires");
                    String expire = expires.get(0);

                    UserDBEntry usr = new UserDBEntry( context );
                    usr.token = token;
                    usr.expire = expire;

                    usr.save();

                    Log.d("TEST", "UserManager receives authentication Token: " + token);
                    callback.onRefreshTokenSuccess(token);

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    callback.onRefreshTokenError(e);
                }
            }
        }

        Link authlink = new Link();
        try {
            Settings st = new Settings(ctx);
            SettingsDBEntry sr = st.getSettingFromKey(Settings.Keys.USER_LOGIN_URL);

            authlink.setHref(sr.value1);
            authlink.setMethod(Methods.POST);
            authlink.setRel(ERelations.LOGIN);
            authlink.setResourceType("application/json");


            String requestBody = "username=" + u.eMail + "&password=" + u.password;

//			String requestBody = "36962803ec2b9b6b7f714b9f7adac619";
            Request request = new Request();
            //request.link = authlink;
            request.setLink(authlink);

            request.setBody(requestBody);
            request.addHeader("Content-Type", "application/x-www-form-urlencoded");
            request.addHeader("Accept", "application/json");

            Log.d("TEST", "UserManager ask for authentication token refresh");
            new RESTTask(request, new ActualRestTaskCallback( ctx )).execute();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            callback.onRefreshTokenError(e);
        }
    }

    public void addEvents(final Context context, Events events, final AddEventsCallback callback) {


        final class URRefreshTokenCallback extends RefreshTokenCallback {

            private Events _events;

            URRefreshTokenCallback(Events events)
            {
                _events = events;
            }

            @Override
            public void onRefreshTokenSuccess(String token) {

                final class LiteAddEventsRestTaskCallback extends RESTTasks.RestTaskCallback {

                    @Override
                    public void onTaskComplete(Response response) {
                        Log.d("TEST", "GetReminders -> onTaskComplete");
                        if (response.hasHTTPException()) {
                            callback.onAddEventsHTTPError(response.getException());
                        } else {

                            callback.onAddEventsSuccess();
                        }
                    }
                }
                Settings settings = new Settings(context);
                SettingsDBEntry entry = settings.getSettingFromKey(Settings.Keys.AGGREGATOR_ENTRY_POINT);
                String aep = entry.value1;
                UserDBEntry udbe = new UserDBEntry(context);
                String userId = udbe.aggregatorID;

                Request request = new Request();
                Link reqlink = new Link();
                reqlink.setMethod(Methods.POST);
                reqlink.setHref(aep + "/users/" + userId + "/events/new");
                request.setLink(reqlink);
                request.addHeader("Authorized-By", token);
                request.addHeader("Content-Type", "application/json");
                request.addHeader("Accept", "application/json");
                String requestBody = null;
                try {
                    requestBody = UserManager.getInstance().eventsToJSON(_events);
                    request.setBody( requestBody );
                    new RESTTask(request, new LiteAddEventsRestTaskCallback() ).execute();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onRefreshTokenHTTPError(HTTPException exception) {
                Log.d("TEST","On refresh token HTTP error Updating reminder");

            }

            @Override
            public void onRefreshTokenError(Exception exception) {
                Log.d("TEST","On refresh token error Updating reminder");
            }
        }


        refreshToken(context, new URRefreshTokenCallback(events) );
    }

    public void getRawDataEventsByExample(final Context context, Event example, final GetEventsCallback callback) {


        final class URRefreshTokenCallback extends RefreshTokenCallback {

            private Event _example;

            URRefreshTokenCallback(Event example)
            {
                _example = example;
            }

            @Override
            public void onRefreshTokenSuccess(String token) {

                final class LiteGetEventsRestTaskCallback extends RESTTasks.RestTaskCallback {

                    @Override
                    public void onTaskComplete(Response response) {
                        //Log.d("TEST", "GetReminders -> onTaskComplete");
                        if (response.hasHTTPException()) {
                            callback.onGetEventsHTTPError(response.getException());
                        } else {

                            RawDataEvents evts = null;
                            Events events = new Events();
                            try {
                                Gson gson = new Gson();
                                evts = (RawDataEvents)gson.fromJson(response.getBody(), RawDataEvents.class);
                                for( RawDataEvent ev : evts.getEvents() ) {
                                    Event e = new Event();
                                    byte[] decoded = Base64.decode(ev.getData(),0);
                                    e.setData( decoded );
                                    e.setType(ev.getType());
                                    e.setStartTime(ev.getStartTime());
                                    e.setEndTime(ev.getEndTime() );
                                    e.setLabel(ev.getLabel());
                                    events.getEvents().add(e);
                                }
                            }
                            catch (Exception e) {
                                Log.e("Error parsing", e.toString());
                                //throw e;
                                callback.onGetEventsSuccess(events);
                            }
                            callback.onGetEventsSuccess(events);
                            //callback.onAddEventsSuccess();
                        }
                    }
                }
                Settings settings = new Settings(context);
                SettingsDBEntry entry = settings.getSettingFromKey(Settings.Keys.AGGREGATOR_ENTRY_POINT);
                String aep = entry.value1;
                UserDBEntry udbe = new UserDBEntry(context);
                String userId = udbe.aggregatorID;

                Request request = new Request();
                Link reqlink = new Link();
                reqlink.setMethod(Methods.POST);
                reqlink.setHref(aep + "/users/" + userId + "/events/example");
                request.setLink(reqlink);
                request.addHeader("Authorized-By", token);
                request.addHeader("Content-Type", "application/json");
                request.addHeader("Accept", "application/json");
                String requestBody = null;
                try {
                    requestBody = UserManager.getInstance().eventToJSON(_example);
                    request.setBody( requestBody );
                    new RESTTask(request, new LiteGetEventsRestTaskCallback() ).execute();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onRefreshTokenHTTPError(HTTPException exception) {
                //Log.d("TEST","On refresh token HTTP error Updating reminder");

            }

            @Override
            public void onRefreshTokenError(Exception exception) {
                Log.d("TEST","On refresh token error Updating reminder");
            }
        }


        refreshToken(context, new URRefreshTokenCallback(example) );
    }

    public void getEventsByExample(final Context context, Event example, final GetEventsCallback callback) {


        final class URRefreshTokenCallback extends RefreshTokenCallback {

            private Event _example;

            URRefreshTokenCallback(Event example)
            {
                _example = example;
            }

            @Override
            public void onRefreshTokenSuccess(String token) {

                final class LiteGetEventsRestTaskCallback extends RESTTasks.RestTaskCallback {

                    @Override
                    public void onTaskComplete(Response response) {
                        //Log.d("TEST", "GetReminders -> onTaskComplete");
                        if (response.hasHTTPException()) {
                            callback.onGetEventsHTTPError(response.getException());
                        } else {

                            Events evts = null;

                            try {
                                Gson gson = new Gson();
                                evts = (Events)gson.fromJson(response.getBody(), Events.class);
                             }
                             catch (Exception e) {
                               Log.e("Error parsing", e.toString());
                               //throw e;
                                 callback.onGetEventsSuccess(evts);
                             }
                             callback.onGetEventsSuccess(evts);
                            //callback.onAddEventsSuccess();
                        }
                    }
                }
                Settings settings = new Settings(context);
                SettingsDBEntry entry = settings.getSettingFromKey(Settings.Keys.AGGREGATOR_ENTRY_POINT);
                String aep = entry.value1;
                UserDBEntry udbe = new UserDBEntry(context);
                String userId = udbe.aggregatorID;

                Request request = new Request();
                Link reqlink = new Link();
                reqlink.setMethod(Methods.POST);
                reqlink.setHref(aep + "/users/" + userId + "/events/example");
                request.setLink(reqlink);
                request.addHeader("Authorized-By", token);
                request.addHeader("Content-Type", "application/json");
                request.addHeader("Accept", "application/json");
                String requestBody = null;
                try {
                    requestBody = UserManager.getInstance().eventToJSON(_example);
                    request.setBody( requestBody );
                    new RESTTask(request, new LiteGetEventsRestTaskCallback() ).execute();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onRefreshTokenHTTPError(HTTPException exception) {
                Log.d("TEST","On refresh token HTTP error Updating reminder");

            }

            @Override
            public void onRefreshTokenError(Exception exception) {
                Log.d("TEST","On refresh token error Updating reminder");
            }
        }


        refreshToken(context, new URRefreshTokenCallback(example) );
    }


}
