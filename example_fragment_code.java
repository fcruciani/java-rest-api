import com.google.gson.Gson;
import com.iplus.RESTLayer.DateUtils;
import com.iplus.RESTLayer.UserManager;
import com.iplus.RESTLayer.callbacks.AddEventsCallback;
import com.iplus.RESTLayer.exceptions.HTTPException;
import com.iplus.RESTLayer.marshalling.model.Event;
import com.iplus.RESTLayer.marshalling.model.Events;
//import your wrapper
import com.serg.rest.LiteClient;


//Utility class for an accelerometer or gyroscope sensor
public class IMUSample {
        double x, y, z;
        long timestamp;
        String descr;
}

//...
 
//here events are tempoararily stored

List<IMUSample> samples_queue = new ArrayList<IMUSample>();

Events  = new Events();


public void onNewSampleRescieved(IMUSample new_sample) {
	//Main loop getting data
	
	//If 60 seconds since last sending process queue sending last minute of data
	//into an event
	//NOTE: you can choose how often send data and how long are event fragements
	//       1 minute in this example. For IMUs 1-10 minutes can work
	if( new_sample.timestamp - last_event_sent_timestamp >= 60000)
		processQueue();
	
	samples_queue.add(new_sample);
	//....
}


public void processQueue() {

	/****************Preparing JSON class to be sent to the aggregator*******************/
	//Events is the aggregator model container for data
	Events events = new Events();


	Event event = new Event();
	ev.setLabel("SHIMMER_ACC");
    ev.setType("RAW_DATA");
    //set start end time interval of data we are sending 
    ev.setStartTime( DateUtils.toISO8601( last_event_sent_timestamp )  );
    ev.setEndTime(DateUtils.toISO8601( last_event_sent_timestamp + 60 )  );
    

    //data can be formatted in any format
    // e.g. you can do 0.0,0.0,1.0,333333;
    //	using ',' as column delimiter ';' as row delimiter 
    // and each row has format ACC_X,ACC_Y,ACC_Z,timestamp 
    String acc_data = new String();
	for( IMUSample sample : samples_queue ){

		acc_data += String.valueOf( sample.x ) + "," + String.valueOf( sample.y ) + "," + String.valueOf( sample.z ) + "," String.valueOf( sample.timestamp ) + ";";

	}

	//Optionally you can encode data in base64
	// it is mandatory if you're sending image data
	// to avoid exception while sending because incompatible encoding
	final byte[] encoded = Base64.encode(acc_bytes, 0);
    e.setData(encoded);

	evs.getEvents().add(ev);


	/************** Defining callback to get response from aggregator ****************************/
	final class ActualAddEventsCallback extends AddEventsCallback
        {
            Context _context;
            Events _evs;
            public ActualAddEventsCallback(Events events,Context context)
            {
                _context = context;
                _evs = events;
            }
            @Override
            public void onAddEventsSuccess() {
                {
                    
                    //IF WE GET HERE EVENT HAVE BEEN SENT CORRECTLY
                    //          :D

                }
            }

            @Override
            public void onAddEventsHTTPError(HTTPException e) {

            	//               :(
                //DO SOMETHIJNG TO DEAL WITH HTTP Exception
                // 401 Unauthorized
                // etc.
            }

            @Override
            public void onAddEventsError(Exception e) {
            	//               :(
                //DO SOMETHING. AN ERROR OCCURED WHILE SENDING
            }
        };


    /// FINALLY we send the list of Events to the aggregator and receive response in the callback
	LiteClient.getInstance().addEvents(context, evs, new ActualAddEventsCallback(evs, context));
}




